import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MainPage from './views/MainPage';
import LoginPage from './views/LoginPage';
import { Provider } from 'react-redux';
import { Store } from './redux/store';

const Stack = createNativeStackNavigator();

export default function App() {

  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen options={{
            title: 'หน้าหลัก'
          }} name="Main" component={MainPage} />
          <Stack.Screen options={{
            title: 'เข้าสู่ระบบ'
          }} name="Login" component={LoginPage} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>

  );
}