import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Text, View, Button } from 'react-native';
import tw from 'tailwind-react-native-classnames';

import { useSelector, useDispatch } from 'react-redux';
import { setUserName, setAge } from '../redux/actions';

export default function LoginPage({ route, navigation }) {

  const { name, age } = useSelector((state: any) => state.userReducer);
  const dispatch = useDispatch();

  React.useEffect(() => {
    console.log('route.params ** ', route.params)
  });

  const setData = async (_name: string, _age: number) => {
    dispatch(setUserName(_name+Date.now()));
    dispatch(setAge(_age+age));
  }



  return (
    <View style={tw`flex-col items-center h-full justify-center`}>
      <Text style={tw`text-red-500 font-bold`}>Login Page</Text>
      <View style={tw`p-10 `}>
        <Text style={tw`text-red-500 font-bold`}>Name: {name}</Text>
        <Text style={tw`text-red-500 font-bold`}>Age: {age}</Text>
      </View>
      <Button title="บันทึกข้อมูล" color="#ffffff" onPress={() => {
        setData('nakarin', 10);
      }} />
      <Button title="หน้าหลัก" onPress={() => {
        navigation.navigate('Main', {
          data: {
            "key": "val" + Date.now()
          }
        })
      }} />
      <StatusBar style="auto" />
    </View>
  );
}