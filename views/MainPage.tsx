import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Text, View, Button } from 'react-native';
import { useSelector } from 'react-redux';
import tw from 'tailwind-react-native-classnames';

export default function MainPage({ route, navigation }): JSX.Element {

    const { name, age } = useSelector((state: any) => state.userReducer);
    React.useEffect(() => {
        console.log('route.params ** ', route.params)
    });

    return (
        <View style={tw`flex-col items-center h-full justify-center `}>
            <Text style={tw`text-red-500 font-bold`}>Main Page</Text>
            <View style={tw`p-10 `}>
                <Text style={tw`text-red-500 font-bold`}>Name: {name}</Text>
                <Text style={tw`text-red-500 font-bold`}>Age: {age}</Text>
            </View>
            <Button title="หน้าเข้าระบบ" onPress={() => {
                navigation.navigate('Login', {
                    data: {
                        "key": "val" + Date.now()
                    }
                })
            }} />
            <StatusBar style="auto" />
        </View>
    );
}