import { SET_USER_NAME, SET_AGE } from './actions';


const initialState = {
    name: '',
    age: 0
}


const userReducer = (state = initialState, action: any) => {
    console.log('userReducer action >> ', action)
    switch (action.type) {
        case SET_USER_NAME:
            return { ...state, name: action.payload };
        case SET_AGE:
            return { ...state, age: action.payload };
        default: 
            return state;
    }
}

export default userReducer;