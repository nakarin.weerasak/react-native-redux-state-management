export const SET_USER_NAME = "SET_USER_NAME";
export const SET_AGE = "SET_AGE";

export const setUserName = (name: string) => (dispatch: any) => {
    dispatch({
        type: SET_USER_NAME,
        payload: name,
    })
}


export const setAge = (age: number) => (dispatch: any) => {
    dispatch({
        type: SET_AGE,
        payload: age,
    })
}